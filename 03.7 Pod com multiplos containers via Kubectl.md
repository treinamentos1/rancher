Simule a criação do Pod e envie a saída em YAML para um manifesto.
```yaml
kubectl run pod-multiplo --image=nginx --dry-run=client -o yaml > pod-multiplo.yaml 
```

Edite e insira mais um container
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-multiplo
  namespace: namespace1
spec:
  containers:
  - image: nginx
    name: nginx
  - image: tomcat:9.0-alpine
    name: tomcat
  restartPolicy: Always
```

Perceba que no campo **metadata** já inserimos também a namespace que o Pod vai ser criado.

Faça o deploy com o comando abaixo:
```yaml
kubectl create -f pod-multiplo.yaml 
```