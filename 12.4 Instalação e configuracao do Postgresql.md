
Procurar versão do Postgres:
```bash
apt policy postgresql
```

Caso não tiver listado o Postgres 14, vamos adicionar o repositório.
```bash
sudo apt install vim curl wget gpg gnupg2 software-properties-common apt-transport-https lsb-release ca-certificates
curl -fsSL https://www.postgresql.org/media/keys/ACCC4CF8.asc|sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/postgresql.gpg
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
sudo apt update
sudo apt install -y postgresql-14 postgresql-client
systemctl status postgresql
```

Permita a autenticação de senha em seu servidor PostgreSQL executando os comandos abaixo.
```bash
sudo sed -i '/^host/s/ident/md5/' /etc/postgresql/14/main/pg_hba.conf
```
Em seguida, altere o método de identificação de ponto para confiança, conforme abaixo.
```bash
sudo sed -i '/^local/s/peer/trust/' /etc/postgresql/14/main/pg_hba.conf
```

Para permitir que a instância seja acessada de qualquer lugar, edite o comando conforme abaixo:
`sudo vim /etc/postgresql/14/main/pg_hba.conf`
No arquivo, adicione as linhas abaixo. 
```bash
# IPv4 local connections:
host    all             all             127.0.0.1/32            scram-sha-256
host    all             all             0.0.0.0/0                md5

# IPv6 local connections:
host    all             all             ::1/128                 scram-sha-256
host    all             all             0.0.0.0/0                md5
```

Agora certifique-se de que o serviço esteja escutando * editando o arquivo conf em /etc/postgresql/14/main/postgresql.conf conforme abaixo.
`sudo vim /etc/postgresql/14/main/postgresql.conf`

No arquivo, remova o comentário e edite a linha conforme abaixo.
```bash
````bash
#------------------------------------------------------------------------------
# CONNECTIONS AND AUTHENTICATION
#-----------------------------------------------------------------------------
.......
listen_addresses='*'
```

Agora reinicie e habilite o PostgreSQL para que as alterações entrem em vigor.
```bash
sudo systemctl restart postgresql
sudo systemctl enable postgresql
```

sudo -u postgres psql
create database wikijs;
create user wikijs with password 'wikijs';
alter user wikijs superuser;
\q


#### Fonte:
- https://computingforgeeks.com/install-postgresql-14-on-ubuntu-jammy-jellyfish/?expand_article=1
