Caso não utilizar certificado
```bash
helm install rancher rancher-stable/rancher \
  --namespace cattle-system \
  --set hostname=rancher.devopshero.click \
  --set bootstrapPassword=admin \
  --version 2.8.2
```

#### Subindo certificado auto-assinado

Crie o secret
```bash
kubectl create secret tls tls-ca --cert=_wildcard.devopshero.click.pem --key=_wildcard.devopshero.click-key.pem -n cattle-system
```

Instale o Rancher incluindo o secret
```bash
helm upgrade --install rancher rancher-stable/rancher \
  --namespace cattle-system \
  --set hostname=rancher.devopshero.click \
  --set bootstrapPassword=admin \
  --set ingress.tls.source=secret \
  --set ingress.tls.secretName=tls-ca \
  --version 2.8.2
```

fonte:
- https://www.youtube.com/watch?v=WwRHqx50XO4&ab_channel=Vovotoqueiro