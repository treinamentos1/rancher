#### Criar o certificado

Instale as dependencias:
```bash
sudo apt install mkcert libnss3-tools
```

Gera o certificado e insira ele no navegado do Linux e confiável no sistema
```bash
mkcert -install *.devopshero.click
```

#### Insira o certificado no kubernetes

Certifique-se de que os arquivos do certificado estão acessíveis no diretório atual ou especifique o caminho completo para eles. Em seguida, execute o comando abaixo para criar o Secret:
```bash
kubectl create secret tls wildcard-devopshero-click-tls \
--cert=_wildcard.devopshero.click.pem \
--key=_wildcard.devopshero.click-key.pem \
-n lab
```

Edite a definição do seu Ingress para referenciar o Secret criado. Aqui está um exemplo de como deve ser a configuração do seu Ingress:
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: exemplo-ingress
  namespace: seu-namespace
spec:
  tls:
  - hosts:
    - upsso.cithyper.click
    secretName: wildcard-devopshero-click-tls #Este Secret
  rules:
  - host: upsso.cithyper.click
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: nome-do-seu-service
            port:
              number: porta-do-seu-service
```