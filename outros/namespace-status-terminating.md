Liste as namespaces:

```bash
kubectl get ns

NAME              STATUS        AGE
default           Active        21h
fleet-default     Terminating   20h
fleet-local       Terminating   20h
```

Crie uma variável com o nome da namespace
```bash
export NAMESPACE=fleet-default 
```

Crie um arquivo
```bash
kubectl get namespace $NAMESPACE -o json > tempfile.json
```

Edite este arquivo:    
`vim tempfile.json`

Remova tudo que tiver dentro de finalizers, de:
```yaml

        "finalizers": [
            "controller.cattle.io/namespace-auth"
        ],
```
para
```yaml
        "finalizers": [
        ],
```

Salve-o e aplique este comando: 
```bash
kubectl proxy &
```

Execute o comando abaixo:
```bash
curl -k -H "Content-Type: application/json" -X PUT --data-binary @tempfile.json http://127.0.0.1:8001/api/v1/namespaces/$NAMESPACE/finalize
```

Liste as namespaces novamente e esta não estará mais na lista.

Fonte: 
- https://stackoverflow.com/questions/52369247/namespace-stucked-as-terminating-how-i-removed-it
- Tópico 18 / 1. Using Curl Command