Deployments é um objeto do Kubernetes que gerencia um conjunto de pods idênticos, garantindo que um número especificado de réplicas do pod esteja em execução a qualquer momento.

```bash
kubectl create deployment meudeploy --image=nginx --replicas=2
```
Para salvar o YAML criado por este comando, acrescente no final da linha `--dry-run=client -o yaml`

Para escalar UP ou Down:
```bash
kubectl scale deployment meudeploy2 --replicas=3
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: meudeploy2
spec:
  template:
    metadata:
      labels:
        app: deploy-nginx
    spec:
      containers:
      - name: container-deploy
        image: nginx 
  replicas: 3
  selector:
    matchLabels:
      app: deploy-nginx
```

Criando o deploy com o manifesto YAML
```bash
kubectl create -f deploy.yaml
```

Verificando o deploy:
```bash
kubectl get deploy
kubectl get rs
kubectl get pod
kubectl get all
```
